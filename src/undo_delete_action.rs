use news_flash::models::{CategoryID, FeedID, TagID};
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Hash, Eq, Clone, Debug, Serialize, Deserialize)]
pub enum UndoDelete {
    Feed(FeedID, String),
    Category(CategoryID, String),
    Tag(TagID, String),
}

impl fmt::Display for UndoDelete {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            UndoDelete::Feed(id, label) => write!(f, "Delete Feed '{}' (id: {})", label, id),
            UndoDelete::Category(id, label) => write!(f, "Delete Category '{}' (id: {})", label, id),
            UndoDelete::Tag(id, label) => write!(f, "Delete Tag '{}' (id: {})", label, id),
        }
    }
}

impl PartialEq for UndoDelete {
    fn eq(&self, other: &Self) -> bool {
        match self {
            UndoDelete::Feed(self_id, _self_title) => match other {
                UndoDelete::Feed(other_id, __other_title) => self_id == other_id,
                UndoDelete::Category(_other_id, __other_title) => false,
                UndoDelete::Tag(_other_id, __other_title) => false,
            },
            UndoDelete::Category(self_id, _title) => match other {
                UndoDelete::Feed(_other_id, __other_title) => false,
                UndoDelete::Category(other_id, __other_title) => self_id == other_id,
                UndoDelete::Tag(_other_id, __other_title) => false,
            },
            UndoDelete::Tag(self_id, _title) => match other {
                UndoDelete::Feed(_other_id, __other_title) => false,
                UndoDelete::Category(_other_id, __other_title) => false,
                UndoDelete::Tag(other_id, __other_title) => self_id == other_id,
            },
        }
    }
}
