using Gtk 4.0;
using Gdk 4.0;
using Adw 1;
using Gio 2.0;

template $VideoDialog : Adw.Window {
  default-height: 200;
  default-width: 200;
  modal: true;

  Overlay main_overlay {
    Overlay {
      Overlay {
        Stack video_stack {
          StackPage {
            name: "spinner";
            child: Spinner {
              halign: center;
              valign: center;
              spinning: true;
              width-request: 32;
              height-request: 32;
            };
          }

          StackPage {
            name: "preview";
            child: Picture preview {
              vexpand: true;
              hexpand: true;
              can-shrink: true;
              content-fit: cover;
            };
          }

          StackPage {
            name: "video";
            child: Overlay {
              Overlay {
                Picture picture {
                  vexpand: true;
                  hexpand: true;
                  can-shrink: true;
                  content-fit: cover;
                }

                [overlay]
                Label buffer_label {
                  visible: false;
                  halign: center;
                  valign: center;
                  styles ["osd", "heading", "toolbar"]
                }
              }
              
              [overlay]
              Label skip_label {
                visible: false;
                halign: center;
                valign: center;
                styles ["osd", "heading", "toolbar"]
              }
            };
          }
        }
        

        [overlay]
        WindowHandle {
          hexpand: true;
          vexpand: true;
        }
      }

      [overlay]
      Box controls {
        height-request: 50;
        margin-bottom: 20;
        margin-top: 20;
        margin-start: 30;
        margin-end: 30;
        orientation: horizontal;
        valign: end;
        halign: fill;
        visible: false;

        EventControllerMotion control_motion {
          propagation-phase: capture;
        }

        Button {
          action-name: "window.stop";
          icon-name: "media-playback-stop";
          styles ["image-button"]
        }

        Button play_button {
          action-name: "window.toggle-play";
          icon-name: "media-playback-start-symbolic";
          styles ["image-button"]
        }

        Box {
          Scale timeline {
            orientation: horizontal;
            hexpand: true;
            sensitive: false;
            adjustment: Adjustment adjustment {
              lower: 0;
              upper: 100;
              value: 0;
            };

            EventControllerMotion timeline_motion_event {
              propagation-phase: capture;
            }
          }

          Popover timeline_popover {
            position: top;
            autohide: false;
            
            Box {
              orientation: vertical;
              spacing: 5;
              Label popover_chapter_label {
                label: "";
              }

              Label popover_time_label {
                label: "";
              }
            }
          }
        }

        DropDown stream_dropdown {
          visible: false;
          sensitive: true;
          show-arrow: false;
          model: Gio.ListStore stream_list_store {};
          factory: Gtk.BuilderListItemFactory list_item_factory {
              template ListItem {
                  child: Label {
                      label: bind template.item as <$NewsFlashVideoStream>.name;
                  };
              }
          };
        }

        Label time_label {
          width-request: 100;
          label: "00:00 / 00:00";
        }

        ScaleButton volume_button {
          adjustment: Adjustment {
            lower: 0.0;
            upper: 1.0;
            value: 1.0;
            step-increment: 0.1;
          };

          EventControllerScroll volume_scroll {
            flags: vertical | discrete;
          }

          GestureClick volume_right_click {
            button: 3;
          }
        }

        styles [ "toolbar", "osd" ]
      }
    }
    

    [overlay]
    Box {
      margin-end: 10;
      margin-top: 10;
      spacing: 10;
      valign: start;
      halign: end;

      Button {
        icon-name: "view-fullscreen-symbolic";
        action-name: "window.fullscreen";

        styles [
          "circular",
          "osd"
        ]
      }

      Button {
        icon-name: "window-close-symbolic";
        action-name: "window.close";

        styles [
          "circular",
          "osd"
        ]
      }
    }
  }

  ShortcutController {
    scope: local;
    propagation-phase: capture;

    Shortcut close_shortcut {
      trigger: "Escape";
    }

    Shortcut play_shortcut {
      trigger: "space";
    }

    Shortcut skip_plus_shortcut {
      trigger: "Right";
    }

    Shortcut skip_minus_shortcut {
      trigger: "Left";
    }
  }

  GestureClick fullscreen_click {
    propagation-phase: capture;
    button: 1;
  }

  EventControllerMotion motion_event {
    propagation-phase: capture;
  }
}
